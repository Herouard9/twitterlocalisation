package devpool.twitter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;
import com.twitter.sdk.android.tweetui.TweetView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

import retrofit2.Call;

public class maps extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    public static  String SEARCH_QUERY = "lpdev";
    public static final String SEARCH_RESULT_TYPE = "top";
    public static final Integer SEARCH_COUNT = 10000;
    public Boolean b1 = true;
    public List<Tweet> tweets;
    public Tweet[] tweetsGeo;
    public TweetView tv;
    public int tweetNb,tweetNb2;
    public Marker[] markerTab;
    Bitmap mImage = null;
    String mediaImageUrl;
    int min = 1;
    int max = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tv = (TweetView) findViewById(R.id.theTweet);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                tv.setVisibility(View.INVISIBLE);
            }
        });

        final SearchService service = Twitter.getApiClient().getSearchService();

        String hashtag = "lpdev";
        Intent intent = getIntent();
        hashtag =  intent.getStringExtra("hashtag");
        SEARCH_QUERY = hashtag;

        Call<Search> tweetsCollect = service.tweets(SEARCH_QUERY, null, null, null, SEARCH_RESULT_TYPE, SEARCH_COUNT, null, null,
                null, b1);

        tweetsCollect.enqueue(new Callback<Search>() {
            @Override
            public void success(Result<Search> searchResult) {

                tweets = searchResult.data.tweets;

                Random r = new Random();
                int i1 = r.nextInt((max - min) + 1) + min;
                double i2 = (double) i1 / (double) 100000 ;

                tweetNb = 0;
                for(int i=0; i<tweets.size() ;i++) {
                    if (tweets.get(i).place != null) {
                        tweetNb++;
                    } else {
                        if (tweets.get(i).coordinates != null) {
                            tweetNb++;
                        }
                    }
                }
                tweetsGeo = new Tweet[tweetNb];
                markerTab = new Marker[tweetNb];
                tweetNb2 = 0;
                for(int i=0; i<tweets.size() ;i++) {
                    if (tweets.get(i).place != null) {
                        Double lon = tweets.get(i).place.boundingBox.coordinates.get(0).get(0).get(0);
                        Double lat = tweets.get(i).place.boundingBox.coordinates.get(0).get(0).get(1);

                        if(tweets.get(i).user.profileImageUrl != null){
                            mediaImageUrl = tweets.get(i).user.profileImageUrl;
                        }
                        Bitmap mediaImage = getBitmapFromURL(mediaImageUrl);

                        //Add markers
                        LatLng point = new LatLng(lat + i2, lon);
                        i1 = r.nextInt((max - min) + 1) + min;
                        i2 = (double) i1 / (double) 100000 ;

                        BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(mediaImage);


                        markerTab[tweetNb2] = mMap.addMarker(new MarkerOptions().position(point).title(tweets.get(i).user.name + " : " + tweets.get(i).text).icon(descriptor));

                        mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                        tweetsGeo[tweetNb2] = tweets.get(i);
                        tweetNb2++;
                    } else {
                        if (tweets.get(i).coordinates != null) {
                            Double lon = tweets.get(i).coordinates.getLongitude();
                            Double lat = tweets.get(i).coordinates.getLatitude();

                            //String mediaImageUrl = tweets.get(i).entities.media.get(0).url;
                            //Bitmap mediaImage = getBitmapFromURL(mediaImageUrl);

                            //Add markers
                            LatLng point = new LatLng(lat + i2, lon);
                            i1 = r.nextInt((max - min) + 1) + min;
                            i2 = (double) i1 / (double) 100000 ;
                            Log.i("bite coord:", String.valueOf(i2));
                            markerTab[tweetNb2] = mMap.addMarker(new MarkerOptions().position(point).title(tweets.get(i).user.name + " : " + tweets.get(i).text));

                            //BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(mediaImage);
                            //markerTab[tweetNb2].setIcon(descriptor);

                            mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                            tweetsGeo[tweetNb2] = tweets.get(i);
                            tweetNb2++;
                        }
                    }
                }
            }

            @Override
            public void failure(TwitterException error) {

            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        for(int i=0; i< this.markerTab.length; i++){
            if(this.markerTab[i].getId().equals(marker.getId())){
                tv.setTweet(tweetsGeo[i]);
                tv.setVisibility(View.VISIBLE);
            }
        }


        return false;
    }

    public void test2TweetLoc(){
        int ecart=1;

        for(int i=0;i<markerTab.length; i++){
            for(int j=0;j<markerTab.length; j++){
                if(markerTab[i].getPosition() ==  markerTab[j].getPosition()){
                    markerTab[j].setPosition(new LatLng(markerTab[j].getPosition().latitude + ecart, markerTab[j].getPosition().longitude));
                    ecart++;
                }
            }
        }
    }

    private Bitmap getBitmapFromURL(final String mediaImageUrl) {
        try {
            Thread t = new Thread() {
                public void run() {
                    try {
                        URL url = new URL(mediaImageUrl);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inScaled = false;
                        mImage = BitmapFactory.decodeStream(input, null, options);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            t.start();
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mImage;
    }
}