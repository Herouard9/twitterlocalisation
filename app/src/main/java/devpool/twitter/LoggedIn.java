package devpool.twitter;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;

import java.util.List;

import retrofit2.Call;


public class LoggedIn extends AppCompatActivity implements View.OnClickListener {

    EditText ed1;
    Button bt1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);
        ed1 = (EditText) findViewById(R.id.editText);
        bt1 = (Button) findViewById(R.id.button);
        bt1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(String.valueOf(ed1.getText()) != null ){
            Log.i("biiiiiiiite : ",String.valueOf(ed1.getText()));
            String hashtag = String.valueOf(ed1.getText());
            Intent intent2 = new Intent(LoggedIn.this, maps.class);
            intent2.putExtra("hashtag",hashtag);
            startActivity(intent2);
        } else {
            Toast.makeText(this.getBaseContext(),"Attention champs de recherche est vide",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
